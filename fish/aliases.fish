## VIM
alias e "nvim"

## LS/TREE/EXA
alias ls "exa"
alias l "exa -l"
alias la "exa -la"
alias tree "exa --tree"
alias t "exa --tree"

## GIT
alias g "git"
alias gd "git diff"
alias gl "git pull"
alias gst "git status"
alias ga "git add"
alias gaa "git add --all"
alias gc "git commit"
alias gp "git push"
alias gyolo="git commit -m (curl -s whatthecommit.com/index.txt)"

## RM
alias rrm "rm -Rf"

## CP
alias cp "cp -v"
alias ccp "cp -R"

## MV
alias mv "mv -v"

## TMUX
alias tmux "tmux -2"
alias tm "tmux -2"
alias at "tmux attach -t"
alias tmls "tmux list-sessions"

## MAKE
alias m "make"
alias mr "make re"

## PYTHON
alias python "python3"
alias p "python3"
alias ip "ipython3"

## CHMOD
alias cx "chmod +x"

## DOCKER
alias d "docker"
alias drm "docker rm"
alias da "docker start"
alias do "docker stop"
alias di "docker images"
alias dps "docker ps"
alias dpsa "docker ps -a"

## DOCKER-COMPOSE
alias dc "docker-compose"
alias dcu "docker-compose up"
alias dcd "docker-compose down"
alias dcps "docker-compose ps"
alias dcpsa "docker-compose ps -a"

## BLIH
alias b "blih"

## BULLSHIT
alias fclol "fortune | cowsay | lolcat"
