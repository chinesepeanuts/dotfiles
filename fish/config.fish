# Get the aliases
source $HOME/.dotfiles/fish/aliases.fish

# Get the functions
source $HOME/.dotfiles/fish/functions.fish

# Disable greeting
set fish_greeting 

# Vi mode
# With cursor shape disabled
function fish_vi_cursor; end
fish_vi_key_bindings

# Default editor is vim
set -gx EDITOR vim

# Set term variable
set -gx TERM xterm-256color

# Colored man
set -gx LESS_TERMCAP_mb (printf "\033[01;31m")
set -gx LESS_TERMCAP_md (printf "\033[01;31m")
set -gx LESS_TERMCAP_me (printf "\033[0m")
set -gx LESS_TERMCAP_se (printf "\033[0m")
set -gx LESS_TERMCAP_so (printf "\033[01;31;33m")
set -gx LESS_TERMCAP_ue (printf "\033[0m")
set -gx LESS_TERMCAP_us (printf "\033[01;32m")

# Wine architechture
set -gx WINEARCH win32

# Add Cargo to PATH
# TODO: Remove this
set -gx PATH $HOME/.cargo/bin $PATH

# Add scripts to PATH
set -gx PATH $HOME/.dotfiles/scripts $PATH

# Locales
set -gx LC_CTYPE fr_FR.UTF-8
set -gx LC_ALL fr_FR.UTF-8

# start_tmux
set -g fish_user_paths "/usr/local/opt/python@2/bin" $fish_user_paths
