Dotfiles
===

NVim, fish, alacritty and tmux are (or were) heavily inspired from [Geospace](https://github.com/Geospace/dotfiles).

You will have to install [qes](https://github.com/koekeishiya/qes) if you want to use my `skhd` configuration. (Now merged)
Using a iTerm2-borderless [patch](https://github.com/jasonwoodland/iTerm2-borderless)

Need to add bitbar/chunkwm plugin.

Beware, those dotfiles can be messy.

These dotfiles are made for a BÉPO layout.

![](./img/bg.png)
Desktop img
