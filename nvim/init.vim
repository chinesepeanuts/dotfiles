"This file is a Neovim configuration file
"It may work on Vim > 8, who knows
"It is a quite heavy configuration

"Plugins
"Managed via vim-plug
"Use :PlugInstall / :PlugClean
"Use :PlugUpdate / :PlugUpgrade
call plug#begin('~/.vim/plugged')
Plug 'chriskempson/base16-vim'
Plug 'python-mode/python-mode', { 'branch': 'develop' }
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'raimondi/delimitmate'
Plug 'lervag/vimtex'
Plug 'scrooloose/nerdtree'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'mileszs/ack.vim'
Plug 'tpope/vim-fugitive'
Plug 'danro/rename.vim'
Plug 'SirVer/ultisnips'
Plug 'ncm2/ncm2'
Plug 'ncm2/ncm2-snippet'
Plug 'ncm2/ncm2-jedi'
Plug 'roxma/nvim-yarp'
Plug 'othree/csscomplete.vim'
Plug 'mattn/emmet-vim'
Plug 'keith/swift.vim'
Plug 'JuliaEditorSupport/julia-vim'
Plug 'w0rp/ale'
Plug 'pangloss/vim-javascript'
Plug 'digitaltoad/vim-pug'
Plug 'dag/vim-fish'
Plug 'junegunn/goyo.vim'
"Plug 'junegunn/limelight.vim'
Plug 'JamshedVesuna/vim-markdown-preview'
Plug 'lervag/vimtex'
call plug#end()

"Remap leader
let mapleader=" "

"Change shell (env variable)
"At least for FZF
let $SHELL = '/bin/bash'

"Colors
"True colors only for compatible terminal emulators
"Dark background (may affet color scheme)
"Enable italics (may require patched terminfo)
"Harder contrast
"Set a colorscheme
if has("termguicolors")
  set termguicolors
endif
set background=dark
colorscheme base16-tomorrow-night 


"Splits
"Split on right and on below
"More natural
"Use :s and :v
set splitright
set splitbelow
cabbrev s split
cabbrev v vsplit

"Cursorline
"Show the cursorline
"set cursorline

"Search
"Search as you type
"Highlight results
"Ignore case
"Use qq to remove the highlight
set incsearch
set hlsearch
set ignorecase
nnoremap qq :nohl<CR>

"Avoid useless redraw
set lazyredraw

"Ignore some extensions in wildmenu
set wildignore+=*.so,*.o,*.swp

"Syntax processing
syntax on

"Buffers integration
"Buffers can be hidden but still alive
"Limit buffer history for speed
"Switch buffers by using ctrl+h/l
set hidden
set history=500
nnoremap <C-Q> :bprev<CR>
nnoremap <C-H> :bnext<CR>

"Identation
filetype plugin indent on
syntax enable
set nosmartindent
set cindent

"Show matching chars
"Like () or {}
set showmatch

"More than 80 chars is bad
"Show a ruller on toggle
nnoremap xx :let &cc = &cc == '' ? '79' : ''<CR>

"Signcolumns
"Always show it
"Avoid any unwanted background
set signcolumn=yes
hi clear SignColumn

"Vertical split color
hi VertSplit guibg=bg

"Status line
"Always show statusline
"Hide the mode indicator as it is built in Airline
"Hide command while typing
set laststatus=2
set noshowmode
set noshowcmd

"Line numbers
"Show linenumbers
"Relative to current line
set number
"set relativenumber

"Line Highlight
highlight LineNr guifg=#464A50 guibg=NONE
highlight CursorLineNr guibg=NONE

"Autocompletion
"Completion with ncm
"Trigger with C-p
"Use tab and S-tab to move between the choices
"Use enter to expand a snippet (see next block)
imap <C-p> <Plug>(cm_force_refresh)
inoremap <expr> <Tab> (pumvisible() ? "\<C-n>" : "\<Tab>")
inoremap <expr> <S-Tab> (pumvisible() ? "\<C-p>" : "\<S-Tab>")
let g:cm_auto_popup=1
set shortmess+=c
set completeopt=menu,menuone,noinsert
set pumheight=5

"Snippets
"UltiSnips is the snippet framework
"Tons of default snippets provided by vim-snippets
"Disable the traditional expand key
"Use a function instead, triggered on enter when completion window is open
"I do so because I use the enter key as my expand key
"and this prevent a bug which makes the key useless otherwise
"Not necessary to remap jump forward/backward as it is
"done on the autocompletion framework
"let g:UltiSnipsJumpForwardTrigger=\"<tab>"
"let g:UltiSnipsJumpBackwardTrigger=\"<S-tab>"
"TODO: Disable default snippets for some languages
let g:UltiSnipsExpandTrigger="<nop>"
let g:ulti_expand_or_jump_res=0
function! <SID>ExpandSnippetOrReturn()
  let snippet=UltiSnips#ExpandSnippetOrJump()
  if g:ulti_expand_or_jump_res > 0
    return snippet
  else
    return "\<CR>"
  endif
endfunction
inoremap <expr> <CR> pumvisible() ?
      \"<C-R>=<SID>ExpandSnippetOrReturn()<CR>" : "\<CR>"

"Quickfix colors
"Hide this ugly yellow
hi QuickFixLine gui=None guibg=None guifg=None

"Ale colors
"Ale is the syntax processing framework
"Intrusive colors
hi clear ALEErrorSign
hi clear ALEWarningSign
hi ALEErrorSign guifg=Red
hi ALEWarningSign guifg=Orange

"Ale signs
let g:ale_set_highlights=0
let g:ale_sign_error='! '
let g:ale_sign_warning='! '

"Linters to use
"If nothing is precised, the default linters are used
let g:ale_linters={
      \'javascript': ['eslint'],
      \'typescript': ['tslint', 'tsserver'],
      \'c': ['clang'],
      \'cpp': ['clang'],
      \}

"Ale C/C++ linting
"Use basic flags
"More specific flags should be provided on a project basis
"by using a local .nvimrc at projet root re-exporting these variables
let g:ale_c_clang_options='-Wall -Wextra -Wshadow --std=gnu11'
let g:ale_cpp_clang_options='-Wall -Wextra -Wshadow --std=gnu++14'

"Ale Python linting
"Override python.vim defaults
"Ignored rules are for 2 spaces indenting
let g:python_recommended_style=0
let g:ale_python_flake8_options='--ignore E111,E114,E121'

"Airline
"Airline is the statusbar framework
"Set a matching colorscheme
"Do not use powerline
"Disable Git tracking
"Mode, Git, Ale errors, Ale warnings, filename
"syntax, file position
let g:airline_theme='base16_grayscale'
let g:airline#extensions#tabline#enabled=0
let g:airline#extensions#tabline#buffer_min_count=2
let g:airline_powerline_fonts=0
let g:airline#extensions#tabline#show_tab_type=0
let g:airline#extensions#default#layout=[
      \ [ 'a', 'error', 'warning', 'b', 'c' ],
      \ [ 'x', 'z' ]
      \ ]
let g:airline#extensions#hunks#enabled=0
let g:airline_section_z=airline#section#create(['%l/%L'])
let g:airline#extensions#tabline#left_sep=''
let g:airline#extensions#tabline#left_alt_sep=''
let g:airline#extensions#tabline#right_sep=''
let g:airline#extensions#tabline#right_alt_sep=''
let g:airline#extensions#whitespace#enabled=0
let g:airline#extensions#ale#error_symbol='❌ '
let g:airline#extensions#ale#warning_symbol='⚠️ '
let g:airline#extensions#tagbar#enabled=0
let g:airline_symbols.readonly='🔒'
let g:airline_symbols.linenr = ''
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.branch = ''

"FZF
"FZF is the fuzzy finder
"Define some shortcuts
"Choose a layout
"Get the good colors
noremap ff :FZF <CR>
let g:fzf_action={
      \ 'ctrl-t': 'tab split',
      \ 'ctrl-s': 'split',
      \ 'ctrl-v': 'vsplit' }
let g:fzf_layout={ 'down': '~30%' }
let g:fzf_colors={
  \ 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'Normal', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment']
  \ }

"And also the buffer switcher
noremap <Leader>j :Buffers <CR>

"And also the commit explorer
noremap <Leader>k :Commits <CR>
noremap <Leader>K :BCommits <CR>

"Nerdtree
"Ignore object and tmp ~files
nnoremap éé :NERDTreeToggle<CR>
let NERDTreeMinimalUI=1
let g:NERDTreeDirArrowExpandable="+"
let g:NERDTreeDirArrowCollapsible="~"
let NERDTreeIgnore=['\.o$', '\~$']
"Nerdtree remap
let NERDTreeMapOpenInTab="è"
let NERDTreeMapOpenInTabSilent="È"
let NERDTreeMapOpenVSplit="v"
let NERDTreeMapRefreshRoot=":"
let NERDTreeMapRefresh="."


"Enable project specific stuff
"This is provided using a local .nvimrc
"Secure avoid loading dangerous stuff
set exrc
set secure

set shiftwidth=2

" [HJKL] -> {CTSR}
noremap c h
noremap r l
noremap t j
noremap s k
noremap C H
noremap R L
noremap T J
noremap S K
noremap zs zj
noremap zt zk
 
" {HJKL} <- [CTSR]
noremap j t
noremap J T
noremap h c
noremap H C
noremap l r
noremap L R
noremap k s
noremap K S
noremap ]k ]s
noremap [k [s

noremap « <
noremap » >
noremap! « <
noremap! » >

" Use ctrl-[ctsr] to select the active split
nmap <silent> gc :wincmd h<CR>
nmap <silent> gr :wincmd l<CR>

"Remap redo
noremap <C-L> <C-R> 

"Markdown preview
let vim_markdown_preview_hotkey='<C-m>'
let vim_markdown_preview_github=1

"Emmet
let g:user_emmet_leader_key='é'

"VimTex	
let g:vimtex_compiler_progname = 'nvr'
let g:vimtex_mappings_enabled = 0

"Ag
let g:ackprg = 'ag --vimgrep --smart-case'   
